﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class ImageMetaData
    {
        [Key]
        public int ID { get; set; }

        [Display(Name ="عنوان عکس")]
        [Required]
        public string Title { get; set; }

        [Display(Name = " عکس")]
        public string ImageName { get; set; }
    }

    [MetadataType(typeof(ImageMetaData))]
    public partial class Image
    {

    }
}
